# Per https://docs.nersc.gov/jobs/best-practices/#improve-efficiency-by-preparing-user-environment-before-running
# set up environment on login node

export PROTOCOL=tcp
echo "PROTOCOL=${PROTOCOL}"

export PARENTDIR=$SCRATCH/python-benchmark/dask-tcp-vs-ucx
cd ${PARENTDIR}

export DASK_DISTRIBUTED__COMM__TIMEOUTS__CONNECT=3600s
export DASK_DISTRIBUTED__COMM__TIMEOUTS__TCP=3600s

sbatch --ntasks 256 ./job.slurm
