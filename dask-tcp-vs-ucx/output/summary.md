# Summary of results

## 4 nodes

### TCP 4 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 16
Other-chunks              | 16
Broadcast                 | default
Protocol                  | tcp
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
Worker thread(s)          | 1
Data processed            | 19.07 GiB
Number of workers         | 16
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
18.92 s                   | 1.01 GiB/s
15.09 s                   | 1.26 GiB/s
14.02 s                   | 1.36 GiB/s
13.17 s                   | 1.45 GiB/s
13.58 s                   | 1.40 GiB/s
14.58 s                   | 1.31 GiB/s
12.76 s                   | 1.50 GiB/s
11.32 s                   | 1.69 GiB/s
14.82 s                   | 1.29 GiB/s
13.35 s                   | 1.43 GiB/s
================================================================================
Throughput                | 1.35 GiB/s +/- 58.63 MiB/s
Bandwidth                 | 55.09 MiB/s +/- 598.40 kiB/s
Wall clock                | 14.16 s +/- 1.90 s
================================================================================
```

### UCX 4 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 16
Other-chunks              | 16
Broadcast                 | default
Protocol                  | ucx
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
TCP                       | None
InfiniBand                | None
NVLink                    | None
Worker thread(s)          | 1
Data processed            | 19.07 GiB
Number of workers         | 16
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
1.57 s                    | 12.12 GiB/s
1.00 s                    | 19.02 GiB/s
1.08 s                    | 17.73 GiB/s
1.05 s                    | 18.11 GiB/s
1.05 s                    | 18.18 GiB/s
1.05 s                    | 18.22 GiB/s
1.04 s                    | 18.42 GiB/s
957.81 ms                 | 19.91 GiB/s
976.16 ms                 | 19.54 GiB/s
1.04 s                    | 18.28 GiB/s
================================================================================
Throughput                | 17.64 GiB/s +/- 886.35 MiB/s
Bandwidth                 | 309.56 MiB/s +/- 9.99 MiB/s
Wall clock                | 1.08 s +/- 167.81 ms
================================================================================
```

## 16 nodes

### TCP 16 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 64
Other-chunks              | 64
Broadcast                 | default
Protocol                  | tcp
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
Worker thread(s)          | 1
Data processed            | 76.29 GiB
Number of workers         | 64
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
57.31 s                   | 1.33 GiB/s
26.38 s                   | 2.89 GiB/s
23.72 s                   | 3.22 GiB/s
24.70 s                   | 3.09 GiB/s
28.17 s                   | 2.71 GiB/s
25.05 s                   | 3.05 GiB/s
23.20 s                   | 3.29 GiB/s
22.78 s                   | 3.35 GiB/s
26.64 s                   | 2.86 GiB/s
24.04 s                   | 3.17 GiB/s
================================================================================
Throughput                | 2.71 GiB/s +/- 305.56 MiB/s
Bandwidth                 | 64.48 MiB/s +/- 257.23 kiB/s
Wall clock                | 28.20 s +/- 9.83 s
================================================================================
```

### UCX 16 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 64
Other-chunks              | 64
Broadcast                 | default
Protocol                  | ucx
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
TCP                       | None
InfiniBand                | None
NVLink                    | None
Worker thread(s)          | 1
Data processed            | 76.29 GiB
Number of workers         | 64
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
5.68 s                    | 13.44 GiB/s
2.06 s                    | 37.06 GiB/s
2.01 s                    | 37.89 GiB/s
1.95 s                    | 39.17 GiB/s
1.98 s                    | 38.55 GiB/s
1.98 s                    | 38.52 GiB/s
2.00 s                    | 38.16 GiB/s
1.96 s                    | 38.85 GiB/s
1.97 s                    | 38.68 GiB/s
2.09 s                    | 36.53 GiB/s
================================================================================
Throughput                | 32.22 GiB/s +/- 4.75 GiB/s
Bandwidth                 | 369.70 MiB/s +/- 2.19 MiB/s
Wall clock                | 2.37 s +/- 1.10 s
================================================================================
```

## 64 nodes

### TCP 64 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 256
Other-chunks              | 256
Broadcast                 | default
Protocol                  | tcp
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
Worker thread(s)          | 1
Data processed            | 305.18 GiB
Number of workers         | 256
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
72.88 s                   | 4.19 GiB/s
21.93 s                   | 13.92 GiB/s
22.12 s                   | 13.79 GiB/s
25.40 s                   | 12.02 GiB/s
26.25 s                   | 11.63 GiB/s
24.00 s                   | 12.71 GiB/s
22.31 s                   | 13.68 GiB/s
24.08 s                   | 12.67 GiB/s
26.53 s                   | 11.50 GiB/s
22.25 s                   | 13.71 GiB/s
================================================================================
Throughput                | 10.61 GiB/s +/- 1.72 GiB/s
Bandwidth                 | 49.78 MiB/s +/- 82.80 kiB/s
Wall clock                | 28.78 s +/- 14.79 s
================================================================================
```

### UCX 64 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 256
Other-chunks              | 256
Broadcast                 | default
Protocol                  | ucx
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
TCP                       | None
InfiniBand                | None
NVLink                    | None
Worker thread(s)          | 1
Data processed            | 305.18 GiB
Number of workers         | 256
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
2.97 s                    | 102.89 GiB/s
6.23 s                    | 48.95 GiB/s
6.12 s                    | 49.86 GiB/s
5.69 s                    | 53.66 GiB/s
6.03 s                    | 50.58 GiB/s
5.62 s                    | 54.25 GiB/s
5.67 s                    | 53.86 GiB/s
5.66 s                    | 53.91 GiB/s
5.58 s                    | 54.74 GiB/s
5.94 s                    | 51.37 GiB/s
================================================================================
Throughput                | 54.98 GiB/s +/- 2.79 GiB/s
Bandwidth                 | 214.10 MiB/s +/- 300.58 kiB/s
Wall clock                | 5.55 s +/- 889.22 ms
================================================================================
```

## 128 nodes

### TCP 128 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 512
Other-chunks              | 512
Broadcast                 | default
Protocol                  | tcp
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
Worker thread(s)          | 1
Data processed            | 610.35 GiB
Number of workers         | 512
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
65.38 s                   | 9.34 GiB/s
33.01 s                   | 18.49 GiB/s
30.78 s                   | 19.83 GiB/s
30.60 s                   | 19.95 GiB/s
33.18 s                   | 18.39 GiB/s
33.93 s                   | 17.99 GiB/s
32.34 s                   | 18.87 GiB/s
32.43 s                   | 18.82 GiB/s
31.66 s                   | 19.28 GiB/s
31.69 s                   | 19.26 GiB/s
================================================================================
Throughput                | 17.19 GiB/s +/- 1.53 GiB/s
Bandwidth                 | 45.67 MiB/s +/- 54.47 kiB/s
Wall clock                | 35.50 s +/- 10.01 s
================================================================================
```

### UCX 128 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 512
Other-chunks              | 512
Broadcast                 | default
Protocol                  | ucx
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
TCP                       | None
InfiniBand                | None
NVLink                    | None
Worker thread(s)          | 1
Data processed            | 610.35 GiB
Number of workers         | 512
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
15.17 s                   | 40.24 GiB/s
14.97 s                   | 40.78 GiB/s
15.56 s                   | 39.23 GiB/s
15.60 s                   | 39.11 GiB/s
15.42 s                   | 39.57 GiB/s
15.44 s                   | 39.54 GiB/s
15.62 s                   | 39.08 GiB/s
16.00 s                   | 38.14 GiB/s
15.36 s                   | 39.72 GiB/s
14.91 s                   | 40.92 GiB/s
================================================================================
Throughput                | 39.62 GiB/s +/- 258.30 MiB/s
Bandwidth                 | 183.33 MiB/s +/- 161.37 kiB/s
Wall clock                | 15.41 s +/- 310.19 ms
================================================================================
```

## 256 nodes

### TCP 256 nodes

Hit timeout, try again

### UCX 256 nodes

```
--------------------------------------------------------------------------------
Backend                   | dask
Merge type                | gpu
Rows-per-chunk            | 40000000
Base-chunks               | 1024
Other-chunks              | 1024
Broadcast                 | default
Protocol                  | ucx
Device(s)                 | 0
RMM Pool                  | True
Frac-match                | 0.6
TCP                       | None
InfiniBand                | None
NVLink                    | None
Worker thread(s)          | 1
Data processed            | 1.19 TiB
Number of workers         | 1024
================================================================================
Wall clock                | Throughput
--------------------------------------------------------------------------------
35.56 s                   | 34.33 GiB/s
37.23 s                   | 32.78 GiB/s
52.68 s                   | 23.17 GiB/s
322.27 s                  | 3.79 GiB/s
322.99 s                  | 3.78 GiB/s
49.38 s                   | 24.72 GiB/s
53.04 s                   | 23.01 GiB/s
48.65 s                   | 25.09 GiB/s
43.90 s                   | 27.81 GiB/s
41.56 s                   | 29.37 GiB/s
================================================================================
Throughput                | 12.12 GiB/s +/- 4.23 GiB/s
Bandwidth                 | 80.10 MiB/s +/- 0.92 MiB/s
Wall clock                | 100.73 s +/- 111.09 s
================================================================================
```
