# Test image for Dask-UCX

Based on [official UCX-Py dockerfiles](https://github.com/rapidsai/ucx-py/tree/branch-0.27/docker)

## Built for NERSC with podman via

```
podman build -t registry.nersc.gov/das/ucx-py:nersc . --file=UCXPy-nersc.dockerfile
```

Uses conda yaml file `ucx-py-nersc.yml` to build environment

Contains custom hashes for
- distributed
- dask-cuda
- ptxcompiler

## Image availble on Perlmutter

```
registry.nersc.gov/das/ucx-py:nersc
```

## To activate the `ucx` environment within the image

Follow the advice on the [official README](https://github.com/rapidsai/ucx-py/blob/branch-0.27/docker/README.md)

and

```
source /opt/conda/etc/profile.d/conda.sh
source /opt/conda/etc/profile.d/mamba.sh
mamba activate ucx
```

