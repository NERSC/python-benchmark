#!/bin/bash
set -ex

CONDA_HOME=${1:-"/opt/conda"}
CONDA_ENV=${2:-"ucx"}

source ${CONDA_HOME}/etc/profile.d/conda.sh
source ${CONDA_HOME}/etc/profile.d/mamba.sh
mamba activate ${CONDA_ENV}

#add modifications needed to run at scale
git clone https://github.com/dask/distributed
cd distributed
git checkout 29dae0230f07bef6f881ec4c6d8ab22dd44d9401
python -m pip install .
cd ..

#wence's changes now merged into the main 22.08 branch
git clone https://github.com/rapidsai/dask-cuda
cd dask-cuda
python -m pip install .
cd ..

git clone https://github.com/rapidsai/ptxcompiler
cd ptxcompiler
git checkout b89103698b9e20faaf74fd6ecc903f202dbdc07e
python -m pip install .
cd ..
