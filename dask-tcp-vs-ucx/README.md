# Benchmarking Dask UCX vs TCP

Largely following the structure [here](https://github.com/wence-/dask-cuda-benchmarks/tree/main/perlmutter)
to make it easier to collaborate.

Some changes to use Shifter containers rather than baremetal
