#!/bin/bash

source /opt/conda/etc/profile.d/conda.sh &&
source /opt/conda/etc/profile.d/mamba.sh &&
mamba activate ucx &&
$@
