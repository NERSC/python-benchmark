module load python
conda create --name openblas python=3.8 -y
source activate openblas
conda install numpy -c conda-forge -y
