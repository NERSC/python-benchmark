import timeit
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--asize', metavar='asize', type=int,
                    help='square dimensions for eigh matrix')

args = parser.parse_args()
asize = args.asize


setup_code='''
import numpy as np
import argparse
import time
parser = argparse.ArgumentParser()

parser.add_argument('--asize', metavar='asize', type=int,
                    help='square dimensions for eigh matrix')

args = parser.parse_args()
asize = args.asize

rng = np.random.default_rng(seed=42)
randarray = rng.random((asize, asize))

'''

test_code=''' 
np.linalg.svd(randarray)
'''
number = 1
repeat = 3
timeit_data = timeit.repeat(stmt=test_code, setup=setup_code, number=number, repeat=repeat)

print('Min runtime (s): {}'.format(min(timeit_data)))
