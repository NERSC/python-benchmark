#!/usr/bin/env bash

module load python
conda create --name mkl python=3.8 -y
source activate mkl
conda install numpy -y 

