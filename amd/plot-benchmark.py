#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 11:48:11 2021

@author: stephey
"""

#let's write a parser for our AMD benchmark data
#should save us the copy/paste pain and make it easy to re-run if we need to

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv("combined_log.out", sep=" ", header=None)

#drop columns we don't need
df_drop = df.drop(columns={df.columns[0], df.columns[2], df.columns[4], df.columns[6], df.columns[8], df.columns[9], df.columns[10]})

#rename columns
df_rename = df_drop.rename(columns={df.columns[1]: 'configuration', df.columns[3]: "library", df.columns[5]: "benchmark", df.columns[7]: "arraysize", df.columns[11]: "runtime"})

#this is ugly but it works

benchmarks = df_rename['benchmark'].unique()
configurations = df_rename['configuration'].unique()
arraysizes = df_rename['arraysize'].unique()
libraries = df_rename['library'].unique()

df_rename['runtime_norm'] = 0 #start here

for b in benchmarks:
    for c in configurations:
        for a in arraysizes:
            #not all arraysizes exist for each benchmark
            try:
                mkl_runtime = float(df_rename[(df_rename['benchmark']==b) & (df_rename['configuration']==c) \
                             & (df_rename['arraysize']==a) & (df_rename['library'] == 'mkl')]['runtime'])  
                                   
                work_norm = float(df_rename[(df_rename['benchmark']==b) & (df_rename['configuration']==c) \
                             & (df_rename['arraysize']==a) & (df_rename['library'] == 'mkl-workaround')]['runtime'])/mkl_runtime
                    
                blas_norm = float(df_rename[(df_rename['benchmark']==b) & (df_rename['configuration']==c) \
                             & (df_rename['arraysize']==a) & (df_rename['library'] == 'openblas')]['runtime'])/mkl_runtime

                #the tricky part is knowing where to put the noramlized data back   
                mkl_loc = df_rename.index[(df_rename['benchmark']==b) & (df_rename['configuration']==c) \
                             & (df_rename['arraysize']==a) & (df_rename['library'] == 'mkl')][0]
                    
                work_loc = df_rename.index[(df_rename['benchmark']==b) & (df_rename['configuration']==c) \
                             & (df_rename['arraysize']==a) & (df_rename['library'] == 'mkl-workaround')][0]
                    
                blas_loc = df_rename.index[(df_rename['benchmark']==b) & (df_rename['configuration']==c) \
                             & (df_rename['arraysize']==a) & (df_rename['library'] == 'openblas')][0]
                    
                #finally insert our normalized values
                df_rename['runtime_norm'].iloc[mkl_loc] = 1
                df_rename['runtime_norm'].iloc[work_loc] = work_norm
                df_rename['runtime_norm'].iloc[blas_loc] = blas_norm
                
            except:
                pass
    
#high AI configuration
high_AI = df_rename[df_rename['configuration']=='highAI']

#high MB configuration
high_MB = df_rename[df_rename['configuration']=='highMB']

#logscale plot for highAI config

g = sns.catplot(x="arraysize", y="runtime", hue="library", col="benchmark", data=high_AI, kind="bar", sharex=False, sharey=False, legend=False)
g.set(yscale="log")
g.set(ylim=(0.05, 1000))
g.set_xlabels('Matrix dimension') 
g.set_ylabels('Min runtime (s)')
g.fig.subplots_adjust(top=0.9) 
g.fig.suptitle('High AI configuration')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

#logscale plot for highMB config

g = sns.catplot(x="arraysize", y="runtime", hue="library", col="benchmark", data=high_MB, kind="bar", sharex=False, sharey=False, legend=False)
g.set(yscale="log")
g.set(ylim=(0.05, 1000))
g.set_xlabels('Matrix dimension') 
g.set_ylabels('Min runtime (s)')
g.fig.suptitle('High MB configuration')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

#linear plot for highAI config

g = sns.catplot(x="arraysize", y="runtime", hue="library", col="benchmark", data=high_AI, kind="bar", sharex=False, sharey=False, legend=False)
g.set_xlabels('Matrix dimension') 
g.set_ylabels('Min runtime (s)')
g.fig.subplots_adjust(top=0.9) 
g.fig.suptitle('High AI configuration')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

#linear plot for highMB config

g = sns.catplot(x="arraysize", y="runtime", hue="library", col="benchmark", data=high_MB, kind="bar", sharex=False, sharey=False, legend=False)
g.set_xlabels('Matrix dimension') 
g.set_ylabels('Min runtime (s)')
g.fig.suptitle('High MB configuration')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

#normalized to MKL highAI config

g = sns.catplot(x="arraysize", y="runtime_norm", hue="library", col="benchmark", data=high_AI, kind="bar", sharex=False, sharey=False, legend=False)
g.set_xlabels('Matrix dimension') 
g.set_ylabels('Normalized runtime')
g.fig.suptitle('High AI configuration')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

#normalized to MKL highMB config

g = sns.catplot(x="arraysize", y="runtime_norm", hue="library", col="benchmark", data=high_MB, kind="bar", sharex=False, sharey=False, legend=False)
g.set_xlabels('Matrix dimension') 
g.set_ylabels('Normalized runtime')
g.fig.suptitle('High MB configuration')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

