# AMD Benchmarking

## Overview

In this repo we'll provide scripts we used to benchmark AMD Rome CPUs
on NERSC's Muller system, a Perlmutter test system. The specific hardware
is a `AMD EPYC 7702 64-Core Processor`.

The objective of these tests are to study Python NumPy performance
on the AMD Rome processor for:

* Intel MKL
* Intel MKL with workaround described [here](https://danieldk.eu/Posts/2020-08-31-MKL-Zen.html)
* OpenBLAS

Our test functions are:

* numpy.linalg.eigh
* numpy.linalg.svd
* numpy.linalg.cholesky
* numpy.dot (matrix dot)

We will test at several matrix sizes appropriate to each problem.

We will test under two configurations:

* Optimized for arithmetic intensity (best dgemm performance)
* Optimized for memory bandwidth (best stream-triad performance)

This is not a perfect test, nor is it exhaustive. Our goal is to understand
general NumPy performance on AMD Rome so we can provide this information to our
users to make informed decisions about which libraries to use on Perlmutter. 

## How to run

1. The test requires an Anaconda Python installation and the ability to create and source conda environments. At NERSC I will use our default Python module. If you are running elsewhere you will need to make sure you have installed Anaconda or Miniconda.
1. You will need to build two conda environments: `mkl` and `openblas`. You can run the corresponding `build-<env>.sh` scripts to generate these environments.
1. You will need to have built the `fakeintel.so` workaround shared object. You can run the `build-fakeintel.sh` script to do this. Note that `run_benchmark.sh` will look for this .so in your current working directory.
1. Once you have completed the first two steps, you can run the script `run_benchmark.sh` on the AMD hardware of your choice. The results will either print to the terminal or can be written to a logfile as specified at the top of the script.
1. `run_benchmark.sh` will loop over all the test environments, test functions, test array sizes, and the two high AI and high MB configurations. It runs each test using timeit 3 times and takes the minimum runtime for each test. Note that the full test will take a few hours to run.
1. If you enabled the logfile, you can parse and plot the results with `plot-benchmark.py`. 
1. You can find results and plots in the `results` directory.


