#!/usr/bin/env bash

SECONDS=0

#set to false if you want stdout
#set to true if you want to reroute to logfile and parse/plot data
log=true

if $log; then
    LOG_FILE=$HOME/logs/log_${SLURM_JOB_ID}.out
    echo "writing log file" $LOG_FILE
    exec 3>&1 1>${LOG_FILE} 2>&1
fi

#load your favorite python
export MODULEPATH=/global/common/software/nersc/cos1.3/extra_modulefiles:$MODULEPATH
module load python

#this script assumes you have built 2 conda environments:
#mkl (see build-mkl.sh)
#openblas (see build-openblas.sh)
#we will hop in and out of them as we do our testing

declare -a LIBRARIES=("mkl" "mkl-workaround" "openblas")
declare -a BENCHMARKS=("eigh_timeit.py" "chol_timeit.py" "svd_timeit.py" "dot_timeit.py")

#loop over libraries
for l in "${LIBRARIES[@]}"	    
    do	
        if [[ "$l" == "mkl-workaround" ]]; then 
            #this assumes you have already built the fakeintel.so (see build-fakeintel.sh)
    	export LD_PRELOAD=$(pwd)/libfakeintel.so
        fi
    
        #mkl and mkl-workaround use the same conda env
        if [[ "$l" == "mkl-workaround" ]]; then
    	#hop in    
            source activate mkl
        else
            source activate $l
        fi

        #loop over requested benchmarks
        for b in "${BENCHMARKS[@]}"
            do
    
            #assign matrix size based on test problem		
            if [[ "$b" == "eigh_timeit.py" ]]; then
                declare -a ARRAYSIZES=("1000" "5000" "10000")
            elif [[ "$b" == "chol_timeit.py" ]]; then
            #100k is too big	
                declare -a ARRAYSIZES=("5000" "10000" "50000")
            elif [[ "$b" == "svd_timeit.py" ]]; then
                declare -a ARRAYSIZES=("1000" "5000" "10000") 
            else [[ "$b" == "dot_timeit.py" ]]
                declare -a ARRAYSIZES=("5000" "10000" "50000")
            fi
    
            #loop over requested arraysizes   
            for s in "${ARRAYSIZES[@]}"
                do
    
                #first run the dgemm, high AI configuration		    
                printf "config: highAI library: $l benchmark: $b arraysize: $s "
                OMP_NUM_THREADS=128 OMP_PLACES=threads OMP_PROC_BIND=close srun -u -n 1 -c 128 --cpu_bind=sockets python -u $b --asize $s
     
                #then run the stream-triad high memory bandwidth configuration
                printf "config: highMB library: $l benchmark: $b arraysize: $s "
                OMP_NUM_THREADS=64 OMP_PLACES=cores OMP_PROC_BIND=spread srun -u -n 1 -c 128 --cpu_bind=sockets python -u $b --asize $s

                done		
            done

           #hop out
           conda deactivate

        done

duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."


