# Case study comparing toy Python application in Shifter, /global/common/software, and $HOME

Here we provide a simple `mpi4py` and `NumPy` app and measure its runtime in
Shifter, on our /global/common/software filesystem, and on our $HOME filesystem
(the default location for most Python conda environments). $HOME is generally
not meant for computing and is not designed for the kinds of file and metadata
access required by jobs. /global/common/software is a better solution since it
is read-only on compute nodes. Shifter is the current NERSC container solution.

Using this simple example, we demonstrate that there are Python performance
differences depending on the user's choice of container or filesystem, largely
as a result of file/metadata access times during package imports. We hope this
may motivate users to choose our more performant options, especially at large
scale. 

## Benchmark overview

The benchmark itself is `mpi4py_numpy_benchmark.py`. It is designed to be used
with `mpi4py`. It exercises several basic features in `NumPy` including
generating a matrix of random integers and finding the matrix max. The purpose
of these operations was to provide the benchmark with some work to do in
addition to just importing libraries. Each rank performs the same amount of
work as the jobsize increases.

We provide all information required to run this benchmark yourself should you
choose to do so.

## Benchmark methods

We have run `mpi4py_numpy_benchmark.py` on 1, 10, 100, and 500 Cori Haswell
nodes. We have run each of these cases on our $HOME filesystem,
/global/common/software filesystem, and in a Shifter container. We have built
each environment to ensure version compatibility and provide the
`requirements.txt` file. We also provide our `Dockerfile` for use in Shifter.
All tests use `NumPy` installed from the conda default channel since it is
built with MKL. pip versions of `NumPy` are built with OpenBLAS which is
typically much slower on Intel systems like Cori.

In all cases we use Cray MPICH. The Python script `mpi4py_numpy_benchmark.py`
was located on /global/common/software for all tests. Each script runs the
benchmark 5 times at the requested jobsize and chosen filesystem/container. We
keep the fastest time and include it here.

## Benchmark results

| Number of nodes    | $HOME               | /global/common/software       | Shifter        |
|--------------------|---------------------|-------------------------------|----------------|
| 1                  | 0m4.256s            | 0m3.894s                      | 0m3.998s       |
| 10                 | 0m10.025s           | 0m4.891s                      | 0m4.274s       |
| 100                | 0m30.790s           | 0m17.392s                     | 0m7.098s       |
| 500                | 4m7.673s            | 0m48.916s                     | 0m14.193s      |

## Discussion

At a jobsize of 1 node, we see minor differences between all three options. At
a jobsize of 10 nodes, we already see that Shifter and /global/common/software
are about 2x as fast as $HOME. At 100 nodes, /global/common/software
remains about 2x as fast as $HOME. Shifter however is about 4x faster than
$HOME. At 500 nodes, /global/common/software is roughly 4x faster than $HOME.
Shifter is roughly 4x faster than /global/common/software and 16x faster than
$HOME.

This small toy example supports our recommendation that users consider using
Shifter at jobsizes larger than 10 nodes. At large scale (100+ nodes), we
strongly urge users to use Shifter. If Shifter is not an option, we suggest
that users consider /global/common/software instead.


